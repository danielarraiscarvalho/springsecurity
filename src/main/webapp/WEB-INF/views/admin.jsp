<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:h="http://xmlns.jcp.org/jsf/html"
	xmlns:ui="http://xmlns.jcp.org/jsf/facelets"
	xmlns:f="http://xmlns.jcp.org/jsf/core" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>P�gina do Administrador</title>
</head>
<body>
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<security:authentication property="principal" var="user" />
		<h1>${user.name} seja bem vindo a �rea de administra��o do
			Sistema!</h1>

		<h4 style="margin: 0; padding: 0;">Seus n�veis de acesso:</h4>
		<ol>
			<c:forEach var="role" items="${user.roles}">
			<li>${role.descricao}</li>
			</c:forEach>
		</ol>


		<c:set var="contextPath" value="${pageContext.request.contextPath}" />
                <a href="${contextPath}/">P�gina Inicial</a>
		<a href="${contextPath}/logout">Sair</a>

	</security:authorize>



</body>
</html>