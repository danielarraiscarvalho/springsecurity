CREATE DATABASE `projetoseminario` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `nivelacesso` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `nivel` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `emailSec` varchar(100) DEFAULT NULL,
  `nascimento` date NOT NULL,
  `nome` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL,
  `sobrenome` varchar(40) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `UK_4tdehxj7dh8ghfc68kbwbsbll` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
CREATE TABLE `nivel_usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_nivel` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_nivel`),
  KEY `FK_a09lvq50wu47vwcks68n8fwh4` (`id_nivel`),
  CONSTRAINT `FK_8ig9vtotuciig3ixpcwhtu8im` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_a09lvq50wu47vwcks68n8fwh4` FOREIGN KEY (`id_nivel`) REFERENCES `nivelacesso` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
